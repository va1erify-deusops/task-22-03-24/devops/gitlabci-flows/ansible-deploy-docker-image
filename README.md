## Ansible репозиторий для деплоя приложеня на контура 

```
├── ansible.cfg
├── inventories
│   ├── dev
│   │   ├── files
│   │   │   └── env.docker-compose
│   │   ├── group_vars
│   │   │   └── all
│   │   │       └── main.yaml
│   │   ├── hosts
│   │   └── templates
│   │       ├── todolist-docker-proxy
│   │       │   └── docker-compose.yml.j2
│   │       └── todolist-traefik
│   │           └── docker-compose.yml.j2
│   └── prod
│       ├── group_vars
│       │   └── all
│       │       └── main.yaml
│       ├── hosts
│       └── templates
│           ├── docker-compose.yml.j2
│           └── env.docker-compose.j2
├── playbook.yaml
├── README.md
└── requirements.yaml
```